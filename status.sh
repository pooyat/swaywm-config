# ~/.config/sway/status.sh
# The Sway configuration file in ~/.config/sway/config calls this script.
# You should see changes to the status bar after saving this script.
# If not, do "killall swaybar" and $mod+Shift+c to reload the configuration.

# Create a JSON object for a statusline item
block () {
    printf "{ \"full_text\": \"%s\",
              \"background\": \"%s\",
              \"border\": \"%s\", 
              \"border_left\": 4,
              \"border_right\": 4,
              \"align\": \"center\",
              \"separator\": false,
              \"separator_block_width\": 0,
              \"markup\": \"pango\" }" "$1" "$2" "$3"
}

# Output version header
# Behold: the newline is necessary here
printf "{ \"version\": 1 }\n"

# Start endless array
printf "["

print_status () {
    date_formatted=$(date "+%a %F")
    time_formatted=$(date "+%H:%M")

    # Acquire status, capcacity, and capacity_level for two batteries
    # status values: "Unknown", "Charging", "Discharging", "Not charging", "Full"
    # capacity: from 0 to 100 (percent)
    # capacity_level values: "Unknown", "Critical", "Low", "Normal", "High", "Full"
    bat0_status=$(cat /sys/class/power_supply/BAT0/status)
    bat0_cap=$(cat /sys/class/power_supply/BAT0/capacity)
    bat0_cap_level=$(cat /sys/class/power_supply/BAT0/capacity_level)
    bat1_status=$(cat /sys/class/power_supply/BAT1/status)
    bat1_cap=$(cat /sys/class/power_supply/BAT1/capacity)
    bat1_cap_level=$(cat /sys/class/power_supply/BAT1/capacity_level)

    # Show average of two batteries. And date.
    # bat_cap is a simple average.  A weighted average could be more accurate
    # (using other parameters in the /sys/class/power_supply/BATx/ directory).
    bat_cap=$(( $(($bat0_cap + $bat1_cap)) / 2 ))
    # battery status dispalyed as: Critical, Low, Battery, Charging, or Full
    # battery status and percentage shown in bold red if Critical or Low
    weight=normal
    background=#212121
    border=#212121
    if [ $bat0_status = Discharging -o $bat1_status = Discharging ]; then
	bat_status=Battery
	if [ $bat0_cap_level = Low -a $bat1_cap_level = Low -o \
			     $bat0_cap_level = Low -a $bat1_cap_level = Critical -o \
			     $bat0_cap_level = Critical -a $bat1_cap_level = Low -o \
			     $bat0_cap_level = Critical -a $bat1_cap_level = Critical ]; then
	    bat_status=Low
	    weight=bold
	    background=#ff0000
	    border=#ff0000
	    if [ $bat0_cap_level = Critical -a $bat1_cap_level = Critical ]; then
		bat_status=Critical
	    fi
	fi
    elif [ $bat0_status = Full -a $bat1_status = Full ]; then
	bat_status=Full
    else
	bat_status=Charging
    fi

    # Start this status
    printf "["

    block "<span weight='$weight'>$bat_status $bat_cap%</span>" "$background" "$border"
    printf ","
    block "$date_formatted" "#333333" "#333333"
    printf ","
    block "$time_formatted" "#414141" "#414141"

    # Finish this status---but the endless array goes on!
    printf "],"
}

while true; do print_status; sleep 2; done
